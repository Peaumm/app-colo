-- phpMyAdmin SQL Dump
-- version 5.1.1deb5ubuntu1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 14, 2024 at 03:04 PM
-- Server version: 8.0.37-0ubuntu0.22.04.3
-- PHP Version: 8.1.2-1ubuntu2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Fouquet-Maxence-Api-colo`
--
CREATE DATABASE IF NOT EXISTS `Fouquet-Maxence-Api-colo` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `Fouquet-Maxence-Api-colo`;

-- --------------------------------------------------------

--
-- Table structure for table `coloc`
--

DROP TABLE IF EXISTS `coloc`;
CREATE TABLE `coloc` (
  `id` int NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `code` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `coloc`
--

INSERT INTO `coloc` (`id`, `name`, `code`) VALUES
(1, 'Les tartiflettes', 'XJSIFGFN'),
(5, 'Solo', 'SFPOIWAZ'),
(6, 'La guilde', 'HMCJRXTX'),
(66, 'Les chatons', 'IRQVVCUL'),
(68, 'Coolocation', 'ILSPGQ2C'),
(70, 'Coda', '0UW8JA36'),
(71, 'Tigy', 'TP6Y3VPG');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` int NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `content` text COLLATE utf8mb4_general_ci NOT NULL,
  `coloc_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `title`, `content`, `coloc_id`) VALUES
(1, 'Salut', 'Alors la c\'est bien', 1),
(2, 'Benoit', 'Obelia', 5),
(3, 'courses', 'penser à prendre du pain ', 71),
(4, 'ménage', 'faire le frigo', 71),
(5, 'liste course', 'du pain avec de la farine', 5),
(6, 'Kilian', 'Mister-DIngo-45', 5),
(7, 'test', 'salam', 5);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments` (
  `id` int NOT NULL,
  `object` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_id_must_receive` int NOT NULL,
  `price` float(7,2) NOT NULL,
  `user_id_must_give` int NOT NULL,
  `untilWhen` date NOT NULL,
  `is_pay` tinyint(1) NOT NULL DEFAULT '0',
  `is_hide` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `object`, `user_id_must_receive`, `price`, `user_id_must_give`, `untilWhen`, `is_pay`, `is_hide`) VALUES
(1, 'Courses', 1, 256.42, 26, '2024-05-15', 1, 1),
(2, 'Courses', 1, 2.42, 27, '2024-06-01', 1, 1),
(4, 'Courses', 29, 25.42, 26, '2024-12-31', 0, 0),
(5, 'Courses', 1, 85.32, 29, '2024-06-01', 1, 1),
(6, 'Courses', 28, 5.90, 1, '2024-06-01', 0, 0),
(7, 'Courses', 29, 20.49, 1, '2024-05-31', 0, 0),
(8, 'Courses', 26, 2.65, 27, '2024-07-04', 0, 0),
(16, 'Courses', 1, 2.00, 27, '2024-06-14', 1, 0),
(17, 'Courses', 1, 2.00, 26, '2024-06-14', 0, 0),
(18, 'Courses', 1, 1.00, 26, '2024-06-20', 1, 0),
(19, 'Courses', 1, 1.00, 27, '2024-06-20', 0, 0),
(20, 'Courses', 1, 1.00, 1, '2024-05-29', 0, 0),
(21, 'Courses', 32, 50.00, 32, '2024-06-06', 0, 0),
(22, 'Courses', 32, 50.00, 1, '2024-06-06', 0, 0),
(23, 'Courses', 35, 0.00, 34, '2024-06-07', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks` (
  `id` int NOT NULL,
  `name` enum('Faire le menage','Faire la vaisselle','Faire les courses','Preparer a manger','Sortir les poubelles') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_id_give_task` int NOT NULL,
  `user_id_receive_task` int NOT NULL,
  `untilWhen` date NOT NULL,
  `finished` tinyint(1) NOT NULL DEFAULT '0',
  `is_hide` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `name`, `user_id_give_task`, `user_id_receive_task`, `untilWhen`, `finished`, `is_hide`) VALUES
(1, 'Faire le menage', 1, 27, '2024-06-20', 0, 0),
(2, 'Faire la vaisselle', 26, 27, '2024-06-06', 1, 1),
(4, 'Faire le menage', 27, 1, '2024-06-06', 1, 1),
(5, 'Faire les courses', 29, 1, '2024-06-12', 1, 1),
(6, 'Faire la vaisselle', 1, 26, '2024-06-14', 0, 0),
(7, 'Faire la vaisselle', 1, 27, '2024-06-14', 0, 0),
(8, 'Preparer a manger', 30, 1, '2024-06-14', 1, 0),
(9, 'Faire les courses', 32, 32, '2024-06-07', 0, 0),
(10, 'Faire le menage', 32, 1, '2024-06-06', 1, 1),
(11, 'Sortir les poubelles', 1, 32, '2024-06-05', 1, 1),
(12, 'Faire la vaisselle', 32, 1, '2025-06-05', 0, 0),
(13, 'Faire le menage', 32, 1, '2024-06-04', 1, 1),
(14, 'Sortir les poubelles', 1, 1, '2024-06-07', 0, 0),
(15, 'Faire la vaisselle', 35, 35, '2024-06-14', 1, 0),
(29, 'Faire les courses', 43, 43, '2024-06-14', 0, 0),
(30, 'Preparer a manger', 1, 1, '2024-06-15', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int NOT NULL,
  `firstname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `lastname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `date_birthday` date NOT NULL,
  `password` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `coloc_id` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `date_birthday`, `password`, `coloc_id`, `created_at`) VALUES
(1, 'Maxence', 'Fouquet', 'test@test.com', '2020-05-21', '$2y$10$8eRNBtKCmRuc5eItraXZWeeg7a1jDsmU758AOb2rLuS69PqUEnpBu', 5, '2024-05-21 07:35:40'),
(26, 'Adria', 'Preston', 'rodew@mailinator.com', '1993-07-25', '$2y$10$IhlTpWc0lzuTLKT9Eog24OxzFbl65FekxqAMBl5wxlZJ9PY.uQmde', 1, '2024-05-27 12:14:06'),
(27, 'Ray', 'Bentley', 'lericy@mailinator.com', '2022-09-06', 'Pa$$w0rd!', 1, '2024-05-27 12:16:50'),
(28, 'David', 'Chatton', 'david.chatton@test.com', '2000-03-15', '$2y$10$uyBc9pj1Jp6WeNVaPL9UWOvNP4VnkUwIZYjIBIFElpawoCUkcJQ1K', 66, '2024-05-29 13:35:36'),
(29, 'Mel', 'us', 'test@gmail.com', '2004-01-14', '$2y$10$xnVueaadF/fNIZbdMBdb.OsSmFcxmw0T6L6bP3lsBwKlSHh6j8sR.', 6, '2024-05-29 16:18:44'),
(30, 'Carter', 'Sloan', 'mopal@mailinator.com', '1970-03-02', '$2y$10$WxvIutCIYbPdfX9/hLtx9ecq7lN4j8FajDKMln2/BkwiUc.s41Sw2', 66, '2024-05-31 07:19:57'),
(31, 'Gray', 'Simmons', 'fifevaw@mailinator.com', '1981-11-29', '$2y$10$vpWsTX.8kw1ynHPYHAeMzuPOhwVv/sm7ZMxfYB50hbmrHrIL20SJ2', NULL, '2024-06-03 19:17:15'),
(32, 'Henry', 'AZERTY', 'a.b@c.d', '2004-02-29', '$2y$10$qONxirFmxI20vBWMitWSCOvkWOwPHB1skAwe7kWTxWx8DIjzB3oTe', 5, '2024-06-05 13:26:46'),
(34, 'maxime', 'juste-lecourt', 'juste.lecourt@gmail.com', '1924-06-06', '$2y$10$dK/8/04/DV/55Wr6ofx5SeqDpmgsOfTrlUp4KW42T25bz8Az80QJ6', 68, '2024-06-06 12:14:28'),
(35, 'Finn', 'Mclean', 'fujoge@mailinator.com', '1972-05-25', '$2y$10$N9pE4Tixfh7PQU7LHklzUebqpzCNXPqQIRmniDQ.950rPtU9FAaym', 68, '2024-06-06 12:16:17'),
(39, 'Larissa', 'Reynolds', 'mutigy@mailinator.com', '1987-11-16', '$2y$10$4QDMuNBKnsw1AoG5VP1oaO.YptIfz.irA5aovy4xgI8Vnq95B3Rka', NULL, '2024-06-07 10:09:04'),
(43, 'Florence', 'Noble', 'tolupazo@mailinator.com', '1975-10-11', '$2y$10$ss71FICF.ihjGCTlZ1BVAej1UrvlKYrXHE/9nmI06NYghXn9W8DpW', 68, '2024-06-10 09:43:46'),
(54, 'Martena', 'Maldonado', 'cywocaboly@mailinator.com', '1971-04-06', '$2y$10$mxvcFwQREKzJES8cqL0svODLYtZ9z2YEC5EFB21MjFHt3KXTPVisG', 68, '2024-06-12 07:24:28'),
(55, 'Jane', 'Lopez', 'liva@mailinator.com', '1978-07-13', '$2y$10$518uB4zv342t9bEaYltFkuNDjhdr/nE21/pzw1pij3fatK24ATkAa', 68, '2024-06-12 07:24:41'),
(56, 'Vanna', 'James', 'zekedaty@mailinator.com', '1991-09-18', '$2y$10$Kh6Y56i5wSEzZuB2E9ImeeRXYmDdrmA7RI6fqMmh1C3dPSNVC31mW', 68, '2024-06-12 07:24:56'),
(57, 'Erin', 'Holmes', 'nulazewu@mailinator.com', '1990-06-16', '$2y$10$QcTv0v7bBW4etcgHw3/ATeEgPsTiZoLwUpSZ0vC3epRp4DE8Q7yAi', 68, '2024-06-12 07:25:18'),
(58, 'Brody', 'Travis', 'xyhyviv@mailinator.com', '2014-07-25', '$2y$10$FtV.q.j7r6TG3US0I9ZY9O8R6vCiek5gvLLViLQWFLVR2j7D0BS6q', 68, '2024-06-12 07:34:43'),
(60, 'Gage', 'Pittman', 'dybuvoxy@mailinator.com', '1985-06-12', '$2y$10$HQpzptD.nWl056G2CYnLbuiv3vH91XkUYgDZlnx46tNhjqxJKYYiK', 66, '2024-06-12 14:26:41'),
(61, 'Christian', 'Stark', 'tukezejo@mailinator.com', '1979-08-18', '$2y$10$0ILmPEUJPsCtdMbhKKnATuAP8cVjmXitJuSMtBAXd/G28NwXqNB3e', NULL, '2024-06-13 07:21:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `coloc`
--
ALTER TABLE `coloc`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `message_coloc_id_foreign_key` (`coloc_id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_user_id_must_receive_foreign_key` (`user_id_must_receive`) USING BTREE,
  ADD KEY `payment_user_id_must_give_foreign_key` (`user_id_must_give`) USING BTREE;

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `task_user_id_give_task_foreign_key` (`user_id_give_task`),
  ADD KEY `task_user_id_receive_task_foreign_key` (`user_id_receive_task`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `users_coloc_id_foreign` (`coloc_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `coloc`
--
ALTER TABLE `coloc`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `message.coloc_id` FOREIGN KEY (`coloc_id`) REFERENCES `coloc` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payment.user_id_must_give` FOREIGN KEY (`user_id_must_give`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `payment.user_id_must_receive` FOREIGN KEY (`user_id_must_receive`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `task.user_id_give_task` FOREIGN KEY (`user_id_give_task`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `task.user_id_receive_task` FOREIGN KEY (`user_id_receive_task`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users.coloc_id` FOREIGN KEY (`coloc_id`) REFERENCES `coloc` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
