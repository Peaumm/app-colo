<h1>🏠 Welcome in Colocat'aire, the apartment-sharing area 🏠</h1>

First, you need to install npm

<h3>Installation NPM</h3>

Use the package manager npm to install helloworld.

Use node v20 use :

```bash
nvm install 20
```
```bash
npm i
```

<h3>Start environnement</h3>

```bash
npm start
```
-----------------------------------------

<h3>Database</h3>

You can find database at the root with the name 

-----------------------------------------

<h3>Different localhost :</h3>

:round_pushpin: **Front:** localhost:5001\
:round_pushpin: **Back:** localhost:6002

-----------------------------------------
<h3>To connect as an account :</h3>

:email: Email : **```test@test.com```**\
:key: Password : **```password```**

:arrow_right: If you want to create a user and you want to join an apartment-sharing, you can use the apartment-sharing's code : **```0UW8JA36```**\
:arrow_right: You can also create an apartment-sharing.

:clipboard: After that, you get redirected to a dashboard.
