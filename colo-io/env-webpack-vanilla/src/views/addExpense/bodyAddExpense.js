export default () => (`
  <div class="container">
    <div class="card text-center">
      <div class="card-header">
        Ajouter une nouvelle dépense
      </div>
      <form id="add-expense-form">
        <div class="card-body">
          <ul>
            <li>
              <div class="input-group mb-3">
                <span for="object" class="input-group-text" id="inputGroup-sizing-default">Objet de la dépense ?</span>
                <input name="object" id="object" type="text" class="form-control">
                </input>
              </div>
              <div class="input-group" id="notUser">
              </div>
            </li>
            <li>
              <div class="input-group mb-3">
                <span for="idGive" class="input-group-text" id="inputGroup-sizing-default">Qui doit vous payer ?</span>
                <select name="idGive" id="idGive" type="select" class="form-control multiple-select" multiple>
                </select>
              </div>
              <div class="input-group" id="notUser">
              </div>
            </li>
            <li>
              <div class="input-group mb-3">
                <span for="price" class="input-group-text" id="inputGroup-sizing-default">Quel est le montant ?</span>
                <input name="price" id="price" type="number" class="form-control" step="0.01">
              </div>
            </li>
            <li>
              <div class="input-group mb-3">
                <span for="date" class="input-group-text" id="inputGroup-sizing-default">Date limite</span>
                <input name="date" id="date" type="date" class="form-control">
              </div>
            </li>
          </ul>
          <button type="submit" class="btn btn-primary">Ajout de la dépense</button>
        </div>
      </form>
    </div>
  </div>
`);
