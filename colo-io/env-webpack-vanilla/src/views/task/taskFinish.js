export default () => (`
  <div class="col" id="parentFinished">
    <div>
      <h2 class="h2common">Tâches terminées</h2>
    </div>
    <div class="card h-100 task" id="finished">
    </div>
  </div>
`);
