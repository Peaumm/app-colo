export default () => (`
    <div class="container">
      <div class="card text-center">
        <div class="card-header">
          Poster un message
        </div>
        <form id="add-message">
          <div class="card-body">
            <ul>
              <li>
                <div class="input-group mb-3">
                  <span for="title" class="input-group-text" id="inputGroup-sizing-default">Titre du message</span>
                  <input name="title" id="title" type="text" class="form-control">
                </div>
              </li>
              <li>
                <div class="input-group mb-3">
                  <span for="content" class="input-group-text" id="inputGroup-sizing-default">Contenu du message</span>
                  <textarea name="content" id="content" class="form-control"></textarea>
                </div>
              </li>
            </ul>
            <button type="submit" class="btn btn-primary">Envoyer</button>
          </div>
        </form>
      </div>
    </div>
  `);
