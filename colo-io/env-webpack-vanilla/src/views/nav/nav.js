import axios from 'axios';

export default async () => {
  let nav;
  const data = sessionStorage.getItem('idUser');
  const response = await axios.get(`http://localhost:6002/user/${data}`);

  if (sessionStorage.getItem('idUser') === null) {
    nav = `
      <div class="navbar-lien col-4 collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link navbar-brand active" aria-current="page" href="/">Connexion</a>
          </li>
          <li class="nav-item ">
            <a class="nav-link navbar-brand" href="/signup">Inscription</a>
          </li>
        </ul>
      </div>
    `;
  } else if (response.data.coloc_id === null) {
    nav = `
      <div class="navbar-lien col-4 collapse navbar-collapse" id="navbarNavDropdown">
        <a class="navbar-brand" href="/disconnect">
          Déconnexion
        </a>
      </div>
    `;
  } else {
    nav = `
      <div class="navbar-lien col-4 collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link navbar-brand" aria-current="page" href="/dashboard">Mon Tableau de bord</a>
          </li>
          <li class="nav-item ">
            <a class="nav-link navbar-brand" href="/profil">Mon Profil</a>
          </li>
          <li class="nav-item ">
            <a class="nav-link navbar-brand" href="/disconnect">Déconnexion</a>
          </li>
        </ul>
      </div>
    `;
  }

  return (`
    <nav class="navbar navbar-expand-lg bg-light" class="row">
      <div class="navbar-title col-1">
        <h1 class="title">Colocat'aire</h1>
      </div>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      ${nav}
    </nav>
  `);
};
