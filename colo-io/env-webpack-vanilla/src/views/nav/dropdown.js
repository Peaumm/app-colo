export default () => (`
  <div class="dropdown navbar-brand">
    <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
      Menu
    </button>
    <ul class="dropdown-menu">
      <li><a class="dropdown-item" href="/task">Tâches ménagère</a></li>
      <li><a class="dropdown-item" href="/expense">Dépense solo</a></li>
      <li><a class="dropdown-item" href="/commonexpense">Dépense commune</a></li>
    </ul>
  </div>
`);

/* <li class="nav-item dropdown">
<a class="nav-link dropdown-toggle" href="#" role="button"
data-bs-toggle="dropdown" aria-expanded="false">
  Menu
</a>
<ul class="dropdown-menu">
  <li><a class="dropdown-item" href="/task">Tâches ménagère</a></li>
  <li><a class="dropdown-item" href="/calendar">Calendrier</a></li>
  <li class="nav-item dropstart">
    <a class="nav-link dropdown-toggle" href="#" role="button"
    data-bs-toggle="dropdown" aria-expanded="false">
      Dépense
    </a>
    <ul class="dropdown-menu">
      <li><a class="dropdown-item" href="/expense">Dépenses solo</a></li>
      <li><a class="dropdown-item" href="#">Dépenses communes</a></li>
    </ul>
  </li>
</ul>
</li> */
