export default () => (`
  <div class="container">
    <div class="card text-center">
      <div class="card-header">
        Création d'une colocation
      </div>
      <form id="new-coloc-form">
        <div class="card-body">
          <ul>
            <li>
              <div class="input-group mb-3">
                <label for="name" class="input-group-text" id="inputGroup-sizing-default">Nom</label>
                <input name="name" id="name" type="text" class="form-control">
              </div>
            </li>
          </ul>
          <button type="submit" class="btn btn-primary">Créer la colocation</button>
        </div>
      </form>
    </div>
  </div>
`);
