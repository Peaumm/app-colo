export default (code) => (`
  <div class="container noMarginBot">
    <div class="row row-cols-1 row-cols-md-1 g-2">
      <div class="col">
        <div class="card h-100">
          <div class="card-body">
            <p class="card-title" id="title-code">Le code de la colocation</p>
            <p class="card-text" id="code">${code}</p>
            <div id="btn-copie">
              <button type="submit" class="btn btn-primary" id="copie">Copier</button>
            </div>
            </div>
        </div>
      </div>
    </div>
    <a href="/dashboard" class="nextTask"><button>Allez au tableau de bord >></button></a>
  </div>
`);
