import house from '../../images/house.png';
import chantier from '../../images/chantier.png';

export default () => (`
  <div class="container">
    <div class="row">
      <div class="col-1"></div>
      <div class="col-lg-4" id="oldColoc">
        <a href="/oldcoloc">
          <button>
            <img class="choose-img" src="${house}"></img>
            <p>Trouver une colocation</p>
          </button>
        </a>
      </div>
      <div class="col-1"></div>
      <div class="col-lg-4">
        <a href="/newcoloc">
          <button>
            <img class="choose-img" src="${chantier}"></img>
            <p>Créer une colocation</p>
          </button>
        </a>
      </div>
    </div>
  </div>
`);
