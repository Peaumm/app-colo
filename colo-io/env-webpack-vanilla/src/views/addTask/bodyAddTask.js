export default () => (`
  <div class="container">
    <div class="card text-center">
      <div class="card-header">
        Ajouter une nouvelle tâche
      </div>
      <form id="add-task-form">
        <div class="card-body">
          <ul>
            <li>
              <div class="input-group mb-3">
                <span for="task" class="input-group-text" id="inputGroup-sizing-default">Quelle est le nom de la tâche?</span>
                <select name="task" id="task" type="select" class="form-control multiple-select-task">
                </select>
              </div>
            </li>
            <li>
              <div class="input-group mb-3">
                <span for="idReceive" class="input-group-text" id="inputGroup-sizing-default">Qui doit faire la tâche?</span>
                <select name="idReceive" id="idReceive" type="select" class="form-control multiple-select">
                </select>
              </div>
              <div class="input-group" id="notUser">
              </div>
            </li>
            <li>
              <div class="input-group mb-3">
                <span for="date" class="input-group-text" id="inputGroup-sizing-default">Date limite</span>
                <input name="date" id="date" type="date" class="form-control">
              </div>
            </li>
          </ul>
          <button type="submit" class="btn btn-primary">Ajout de la tâche</button>
        </div>
      </form>
    </div>
  </div>
`);
