export default () => (`
  <div class="container noMarginBot">
    <div class="yellowOrWhite">
      <h3>Mes paiements à venir</h3>
    </div>
    <div class="row row-cols-1 row-cols-md-1 g-2">
      <div class="col">
        <div class="card h-100" id="sold">
        </div>
      </div>
    </div>
    <div id="btnDashboard">
      <a href="/addexpense" class="nextTask"><button id="prettyButton">Nouvelle dépense</button></a>
      <a href="/commonexpense" class="nextTask"><button id="prettyButton">Voir mon solde</button></a>
    </div>
  </div>
`);
