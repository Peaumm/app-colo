export default () => (`
  <div class="container">
    <h2 class="text-mid yellowOrWhite">Mes derniers messages <a href="/message"><button id="prettyButton" class="sendButton"><i class="fa-solid fa-paper-plane"></i></button></a></h2>
    <div class="row row-cols-1 row-cols-md-1 g-2">
      <div class="col">
        <div id="messages" class="card h-100">
        </div>
      </div>
    </div>
  </div>
`);
