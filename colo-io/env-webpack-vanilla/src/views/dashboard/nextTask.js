export default () => (`
  <div class="container noMarginBot">
    <div class="yellowOrWhite">
      <h3>Mes Prochaines tâches</h3>
    </div>
    <div class="row row-cols-1 row-cols-md-1 g-2">
      <div class="col">
        <div class="card h-100 color" id="task">
        </div>
      </div>
    </div>
    <div id="btnDashboard">
      <a href="/addtask" class="nextTask"><button id="prettyButton">Nouvelle tâche</button></a>
      <a href="/task" class="nextTask"><button id="prettyButton">Voir toutes mes tâches</button></a>
    </div>
    
  </div>
`);
