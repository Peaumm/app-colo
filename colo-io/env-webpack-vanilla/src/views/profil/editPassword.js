export default () => (`
  <div class="container noMarginBot littleMarginTop">
    <div class="card text-center">
      <div class="card-header">
        Modifier mon mot de passe
      </div>
      <form id="password-form">
        <div class="card-body">
        <div id="error"></div>
          <ul>
            <li>
              <div class="input-group mb-3">
                <span for="oldPassword" class="input-group-text" id="inputGroup-sizing-default">Ancien Mot de passe</span>
                <input name="oldPassword" id="oldPassword" type="password" class="form-control">
              </div>
            </li>
            <li>
              <div class="input-group mb-3">
                <span for="newPassword" class="input-group-text" id="inputGroup-sizing-default">Nouveau Mot de passe</span>
                <input name="newPassword" id="newPassword" type="password" class="form-control">
              </div>
            </li>
            <li>
              <div class="input-group mb-3">
                <span for="reNewPassword" class="input-group-text" id="inputGroup-sizing-default">Comfirmer Nouveau Mot de passe</span>
                <input name="reNewPassword" id="reNewPassword" type="password" class="form-control">
              </div>
            </li>
          </ul>
          <button type="submit" class="btn btn-primary">Modifier mon Mot de passe</button>
        </div>
      </form>
    </div>
  </div>
`);
