export default (datas) => {
  const userFirstname = datas.firstname;
  const userLastname = datas.lastname;
  const userEmail = datas.email;
  const date = datas.age;
  const colocId = datas.coloc_id;
  const colocCode = datas.coloc_code;

  return (`
    <div class="container noMarginBot littleMarginTop">
      <div class="card text-center">
        <div class="card-header">
          Mes Informations personnelles
        </div>
        <form id="info-form">
          <div class="card-body">
            <ul>
              <li>
                <div class="input-group mb-3">
                  <span for="firstname" class="input-group-text" id="inputGroup-sizing-default">Prénom</span>
                  <input name="firstname" id="firstname" type="text" class="form-control" value="${userFirstname}" required>
                </div>
              </li>
              <li>
                <div class="input-group mb-3">
                  <span for="lastname" class="input-group-text" id="inputGroup-sizing-default">Nom</span>
                  <input name="lastname" id="lastname" type="text" class="form-control" value="${userLastname}" required>
                </div>
              </li>
              <li>
                <div class="input-group mb-3">
                  <span for="email"class="input-group-text" id="inputGroup-sizing-default">Email</span>
                  <input name="email" id="email" type="text" class="form-control" value="${userEmail}" required>
                </div>
              </li>
              <li>
                <div class="input-group mb-3">
                  <span for="date"class="input-group-text" id="inputGroup-sizing-default">Âge</span>
                  <input name="date" id="date" type="date" class="form-control" value="${date}" required>
                </div>
              </li>
              <div class="row">
                <div class="col-lg-6">
                  <li>
                    <div class="input-group mb-3">
                      <span class="input-group-text" id="inputGroup-sizing-default">Nom de la colocation</span>
                      <input type="text" class="form-control" value="${colocId}" disabled>
                    </div>
                  </li>
                </div>
                <div class="col-lg-6">
                  <li>
                    <div class="input-group mb-3">
                      <span class="input-group-text" id="inputGroup-sizing-default">Code de la colocation</span>
                      <input type="text" class="form-control" value="${colocCode}" disabled>
                    </div>
                  </li>
                </div>
              </div>
            </ul>
            <button type="submit" class="btn btn-primary">Modifier</button>
          </div>
        </form>
      </div>
    </div>
  `);
};
