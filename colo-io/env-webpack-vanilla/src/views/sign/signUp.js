export default () => (`
  <div class="container">
    <div class="card text-center">
      <div class="card-header">
        Inscription
      </div>
      <form id="register-form">
        <div class="card-body">
          <div id="error"></div>
          <ul>
            <li>
              <div class="input-group mb-3">
                <label for="firstname" class="input-group-text" id="inputGroup-sizing-default">Prénom</label>
                <input name="firstname" id="firstname" type="text" class="form-control" required>
              </div>
            </li>
            <li>
              <div class="input-group mb-3">
                <label for="lastname" class="input-group-text" id="inputGroup-sizing-default">Nom</label>
                <input name="lastname" id="lastname" type="text" class="form-control" required>
              </div>
            </li>
            <li>
              <div class="input-group mb-3">
                <label for="email" class="input-group-text" id="inputGroup-sizing-default">Email</label>
                <input name="email" id="email" type="text" class="form-control" required>
              </div>
            </li>
            <li>
              <div class="input-group mb-3">
                <label for="age" class="input-group-text" id="inputGroup-sizing-default">Date de naissance</label>
                <input name="age" id="age" type="date" class="form-control" required>
              </div>
            </li>
            <li>
              <div class="input-group mb-3">
                <label for="password" class="input-group-text" id="inputGroup-sizing-default">Mot de passe</label>
                <input name="password" id="password" type="password" class="form-control" required>
              </div>
            </li>
            <li>
              <div class="input-group mb-3">
                <label for="confirmPassword" class="input-group-text" id="inputGroup-sizing-default">Confirmer le Mot de passe</label>
                <input name="confirmPassword" id="confirmPassword" type="password" class="form-control" required>
              </div>
            </li>
          </ul>
          <button id="submit" type="submit" class="btn btn-primary">Inscription</button>
        </div>
      </form>
      <div class="card-footer text-body-secondary">
        <a href="/"><button type="submit" class="btn btn-dark">Déjà un compte ? Connecte toi ici</button></a>
      </div>
    </div>
  </div>
`);
