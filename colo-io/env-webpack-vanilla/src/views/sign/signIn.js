export default () => (`
  <div class="container">
    <div class="card text-center">
      <div class="card-header">
        Connexion
      </div>
      <form id="connect-form">
        <div class="card-body">
          <div id="error"></div>
          <ul>
            <li>
              <div class="input-group mb-3">
                <label for="email" class="input-group-text" id="inputGroup-sizing-default">Email</label>
                <input name="email" id="email" type="text" class="form-control" required>
              </div>
            </li>
            <li>
              <div class="input-group mb-3">
                <label for="password" class="input-group-text" id="inputGroup-sizing-default">Mot de passe</label>
                <input name="password" id="password" type="password" class="form-control" required>
              </div>
            </li>
          </ul>
          <button id="submit" type="submit" class="btn btn-primary">Connexion</button>
        </div>
      </form>
      <div class="card-footer text-body-secondary">
        <a href="/signup"><button type="submit" class="btn btn-dark">Pas encore de compte ? Inscris toi ici</button></a>
      </div>
    </div>
  </div>

`);
