export default (name) => (`
  <div class="container noMarginBot">
    <div class="row row-cols-1 row-cols-md-1 g-2">
      <div class="col">
        <div class="card h-100">
          <div class="card-body">
            <p class="card-title" id="title-code">Bienvenue</p>
            <p class="card-text" id="code">Vous avez rejoint ${name}</p>
            </div>
        </div>
      </div>
    </div>
    <a href="/dashboard" class="nextTask"><button>Allez au tableau de bord >></button></a>
  </div>
`);
