export default () => (`
  <div class="container">
    <div class="card text-center">
      <div class="card-header">
        Rejoindre une colocation
      </div>
      <form id="old-coloc-form">
        <div class="card-body">
          <ul>
            <li>
              <div class="input-group mb-3">
                <label for="code" class="input-group-text" id="inputGroup-sizing-default">Code de la colocation</label>
                <input name="code" id="code" type="text" class="form-control">
              </div>
            </li>
          </ul>
          <button type="submit" class="btn btn-primary">Rejoindre</button>
        </div>
      </form>
    </div>
  </div>
`);
