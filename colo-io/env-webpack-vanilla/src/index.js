import Router from './router';
import Connect from './controllers/Connect';
import Register from './controllers/Register';
import Dashboard from './controllers/Dashboard';
import CommonExpense from './controllers/CommonExpense';
import Profil from './controllers/Profil';
import Task from './controllers/Task';
import AddExpense from './controllers/AddExpense';
import AddTask from './controllers/AddTask';
import Disconnect from './controllers/Disconnect';
import ChooseColoc from './controllers/ChooseColoc';
import NewColoc from './controllers/NewColoc';
import OldColoc from './controllers/OldColoc';
import ImportantMessage from './controllers/ImportantMessage';

import './index.scss';

const routes = [{
  url: '/',
  controller: Connect
}, {
  url: '/signup',
  controller: Register
}, {
  url: '/dashboard',
  controller: Dashboard
}, {
  url: '/commonexpense',
  controller: CommonExpense
}, {
  url: '/profil',
  controller: Profil
}, {
  url: '/task',
  controller: Task
}, {
  url: '/addexpense',
  controller: AddExpense
}, {
  url: '/addtask',
  controller: AddTask
}, {
  url: '/disconnect',
  controller: Disconnect
}, {
  url: '/choose',
  controller: ChooseColoc
}, {
  url: '/newcoloc',
  controller: NewColoc
}, {
  url: '/oldcoloc',
  controller: OldColoc
}, {
  url: '/message',
  controller: ImportantMessage
}];

new Router(routes);
