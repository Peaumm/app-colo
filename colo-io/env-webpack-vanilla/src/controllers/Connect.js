import axios from 'axios';

import viewNav from '../views/nav/nav';
import signIn from '../views/sign/signIn';

const Connect = class Connect {
  constructor(params) {
    this.el = document.querySelector('#root');
    this.params = params;
    this.data = [];

    this.run();
  }

  async sendData() {
    const elForm = document.querySelector('#connect-form');
    const datasForm = new FormData(elForm);
    const elError = document.querySelector('#error');
    let msgError = '';

    try {
      const response = await axios.get('http://localhost:6002/users');

      response.data.forEach(async (user) => {
        if (user.email === datasForm.get('email')) {
          const datas = {
            id: user.id,
            password: datasForm.get('password')
          };
          const passwordHash = await axios.post(
            'http://localhost:6002/passwordverify',
            datas
          );
          if (passwordHash.data === '200') {
            msgError = '';
            const idUser = user.id;
            sessionStorage.setItem('idUser', idUser);
            window.location.href = 'http://127.0.0.1:5001/choose';
          } else {
            msgError = 'Mauvais Mot de passe';
          }
        } else {
          msgError = "Cet identifiant n'existe pas";
        }
        if (msgError !== '') {
          elError.innerHTML = msgError;
        }
      });
      return 0;
    } catch (error) {
      return ('Error sending message to database:', error);
    }
  }

  submitForm() {
    const elForm = document.querySelector('#connect-form');

    elForm.addEventListener('submit', (e) => {
      e.preventDefault();
      this.sendData();
    });
  }

  async render() {
    return `
      <div>${await viewNav()}</div>
      ${signIn()}
    `;
  }

  async run() {
    this.el.innerHTML = await this.render();
    this.submitForm();
  }
};

export default Connect;
