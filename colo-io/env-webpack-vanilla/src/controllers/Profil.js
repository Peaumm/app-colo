import axios from 'axios';
import viewNav from '../views/nav/nav';
import personalInfo from '../views/profil/personalInfo';
import editPassword from '../views/profil/editPassword';

const Profil = class Profil {
  constructor(params) {
    this.el = document.querySelector('#root');
    this.params = params;
    this.data = [];

    this.run();
  }

  async info() {
    const id = parseInt(sessionStorage.getItem('idUser'), 10);
    const user = await axios.get(`http://localhost:6002/user/${id}`);
    const coloc = await axios.get(`http://localhost:6002/coloc/${user.data.coloc_id}`);
    const datas = {
      firstname: user.data.firstname,
      lastname: user.data.lastname,
      email: user.data.email,
      age: user.data.date_birthday,
      coloc_id: coloc.data.name,
      coloc_code: coloc.data.code
    };
    return datas;
  }

  async sendDataUser() {
    const elForm = document.querySelector('#info-form');
    const datasForm = new FormData(elForm);

    try {
      const id = parseInt(sessionStorage.getItem('idUser'), 10);
      const datas = {
        firstname: datasForm.get('firstname'),
        lastname: datasForm.get('lastname'),
        email: datasForm.get('email'),
        date_birthday: datasForm.get('date')
      };
      const response = await axios.post(
        `http://localhost:6002/user/${id}`,
        datas
      );
      if (response.status === 200) {
        window.location.href = 'http://127.0.0.1:5001/profil';
      }
      return 0;
    } catch (error) {
      return ('Error sending message to database:', error);
    }
  }

  submitFormUser() {
    const elForm = document.querySelector('#info-form');

    elForm.addEventListener('submit', (e) => {
      e.preventDefault();
      this.sendDataUser();
    });
  }

  async sendDataPassword() {
    const elForm = document.querySelector('#password-form');
    const datasForm = new FormData(elForm);
    const idUser = parseInt(sessionStorage.getItem('idUser'), 10);
    const elError = document.querySelector('#error');
    let msgError;

    try {
      const passwordVerify = {
        id: idUser,
        password: datasForm.get('oldPassword')
      };
      const verifyPass = await axios.post(
        'http://localhost:6002/passwordverify',
        passwordVerify
      );
      if (verifyPass.data === '200') {
        if (datasForm.get('newPassword') === datasForm.get('reNewPassword')) {
          const datas = {
            password: datasForm.get('newPassword')
          };
          const response = await axios.post(
            `http://localhost:6002/userpassword/${idUser}`,
            datas
          );
          if (response.data === 403) {
            msgError = 'Le mot de passe doit contenir au moins 8 caractères';
          } else if (response.status === 200) {
            msgError = '';
            window.location.href = 'http://127.0.0.1:5001/profil';
          }
        } else {
          msgError = 'Les 2 Mots de passe ne correspondent pas';
        }
      } else {
        msgError = "L'ancien Mot de passe n'est pas le bon";
      }
      elError.innerHTML = msgError;
      return 0;
    } catch (error) {
      return ('Error sending message to database:', error);
    }
  }

  submitFormPassword() {
    const elForm = document.querySelector('#password-form');

    elForm.addEventListener('submit', (e) => {
      e.preventDefault();
      this.sendDataPassword();
    });
  }

  async render() {
    return `
      <div>${await viewNav()}</div>
      ${personalInfo(await this.info())}
      ${editPassword()}
    `;
  }

  async run() {
    this.el.innerHTML = await this.render();
    this.submitFormUser();
    this.submitFormPassword();
  }
};

export default Profil;
