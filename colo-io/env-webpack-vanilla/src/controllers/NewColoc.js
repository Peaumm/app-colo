import axios from 'axios';
import viewNav from '../views/nav/nav';
import body from '../views/newColoc/body';
import newCode from '../views/newColoc/newCode';

const NewColoc = class NewColoc {
  constructor(params) {
    this.el = document.querySelector('#root');
    this.params = params;
    this.data = [];

    this.run();
  }

  randomCode() {
    let code = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    const charactersLength = characters.length;
    let counter = 0;
    while (counter < 8) {
      code += characters.charAt(Math.floor(Math.random() * charactersLength));
      counter += 1;
    }
    return code;
  }

  async sendData() {
    const elForm = document.querySelector('#new-coloc-form');
    const datasForm = new FormData(elForm);

    const randomPass = this.randomCode();

    try {
      const datasColoc = {
        name: datasForm.get('name'),
        code: randomPass
      };
      const response = await axios.post(
        'http://localhost:6002/colocs',
        datasColoc
      );
      const datasColocId = {
        coloc_id: response.data.id,
        id: parseInt(sessionStorage.getItem('idUser'), 10)
      };
      await axios.post(
        'http://localhost:6002/usercolocid',
        datasColocId
      );
      const displayCode = document.querySelector('#code');
      displayCode.innerHTML = newCode(randomPass);
      this.copiedText(randomPass);

      return 0;
    } catch (error) {
      return ('Error sending message to database:', error);
    }
  }

  submitForm() {
    const elForm = document.querySelector('#new-coloc-form');

    elForm.addEventListener('submit', (e) => {
      e.preventDefault();
      this.sendData();
    });
  }

  copiedText(code) {
    const button = document.querySelector('#copie');

    button.addEventListener('click', () => {
      navigator.clipboard.writeText(code);
      button.innerText = 'Copié !';
    });
  }

  async render() {
    return `
      <div>${await viewNav()}</div>
      ${body()}
      <div class="container" id="code">
        <div class="container addExpense">
          <a href="/choose"><button id="prettyButton">Retour</button></a>
        </div>
      </div>
    `;
  }

  async run() {
    this.el.innerHTML = await this.render();
    this.submitForm();
  }
};

export default NewColoc;
