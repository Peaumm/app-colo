import axios from 'axios';
import viewNav from '../views/nav/nav';
import body from '../views/oldColoc/body';
import message from '../views/oldColoc/message';

const OldColoc = class OldColoc {
  constructor(params) {
    this.el = document.querySelector('#root');
    this.params = params;
    this.data = [];

    this.run();
  }

  async sendData() {
    const elForm = document.querySelector('#old-coloc-form');
    const datasForm = new FormData(elForm);

    try {
      const dataCode = datasForm.get('code');
      const response = await axios.get('http://localhost:6002/colocs');

      response.data.forEach(async (datas) => {
        if (datas.code === dataCode) {
          const datasColocId = {
            coloc_id: datas.id,
            id: parseInt(sessionStorage.getItem('idUser'), 10)
          };
          await axios.post(
            'http://localhost:6002/usercolocid',
            datasColocId
          );
          const displayMessage = document.querySelector('#message');
          displayMessage.innerHTML = message(datas.name);
        }
      });
      return 0;
    } catch (error) {
      return ('Error sending message to database:', error);
    }
  }

  submitForm() {
    const elForm = document.querySelector('#old-coloc-form');

    elForm.addEventListener('submit', (e) => {
      e.preventDefault();
      this.sendData();
    });
  }

  async render() {
    return `
      <div>${await viewNav()}</div>
      ${body()}
      <div class="container" id="message"> 
        <div class="container addExpense">
          <a href="/choose"><button id="prettyButton">Retour</button></a>
        </div>
      </div>
    `;
  }

  async run() {
    this.el.innerHTML = await this.render();
    this.submitForm();
  }
};

export default OldColoc;
