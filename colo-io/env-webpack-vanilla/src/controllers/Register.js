import axios from 'axios';
import viewNav from '../views/nav/nav';
import signUp from '../views/sign/signUp';

const Register = class Register {
  constructor(params) {
    this.el = document.querySelector('#root');
    this.params = params;
    this.data = [];

    this.run();
  }

  async sendData() {
    const elForm = document.querySelector('#register-form');
    const datasForm = new FormData(elForm);
    const elError = document.querySelector('#error');
    let msgError = '';

    try {
      if (datasForm.get('password') === datasForm.get('confirmPassword')) {
        const firstnameUpper = `${datasForm.get('firstname').charAt(0).toUpperCase()}${datasForm.get('firstname').substring(1)}`;
        const lastnameUpper = `${datasForm.get('lastname').charAt(0).toUpperCase()}${datasForm.get('lastname').substring(1)}`;

        const datas = {
          firstname: firstnameUpper,
          lastname: lastnameUpper,
          email: datasForm.get('email'),
          date_birthday: datasForm.get('age'),
          password: datasForm.get('password')
        };
        const response = await axios.post(
          'http://localhost:6002/users',
          datas
        );
        if (response.data === 403) {
          msgError = 'Le mot de passe doit contenir au moins 8 caractères';
        } else if (response.status === 200) {
          sessionStorage.setItem('idUser', response.data.id);
          window.location.href = 'http://127.0.0.1:5001/choose';
        }
      } else {
        msgError = 'Les 2 mots de passe ne correspondent pas';
      }
      elError.innerHTML = msgError;
      return 0;
    } catch (error) {
      return ('Error sending message to database:', error);
    }
  }

  submitForm() {
    const elForm = document.querySelector('#register-form');

    elForm.addEventListener('submit', (e) => {
      e.preventDefault();
      this.sendData();
    });
  }

  async render() {
    return `
      <div>${await viewNav()}</div>
      ${signUp()}
    `;
  }

  async run() {
    this.el.innerHTML = await this.render();
    this.submitForm();
  }
};

export default Register;
