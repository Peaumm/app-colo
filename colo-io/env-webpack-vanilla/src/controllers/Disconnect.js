const Disconnect = class Disconnect {
  constructor(params) {
    this.el = document.querySelector('#root');
    this.params = params;
    this.data = [];

    this.run();
  }

  clearSession() {
    sessionStorage.clear();
    window.location.href = 'http://127.0.0.1:5001/';
  }

  run() {
    this.clearSession();
  }
};

export default Disconnect;
