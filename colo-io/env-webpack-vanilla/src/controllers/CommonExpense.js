import axios from 'axios';
import Chart from 'chart.js/auto';
import viewNav from '../views/nav/nav';
import btnAddExpense from '../views/expense/btnAddExpense';
import graph from '../views/expense/graph';
import resumeSold from '../views/expense/resumeSold';
import followUp from '../views/expense/followUp';

const CommonExpense = class CommonExpense {
  constructor(params) {
    this.el = document.querySelector('#root');
    this.params = params;
    this.data = [];

    this.run();
  }

  dateDiff(date1, date2) {
    const diff = {};
    let tmp = date2 - date1;

    tmp = Math.floor(tmp / 1000);
    diff.sec = tmp % 60;

    tmp = Math.floor((tmp - diff.sec) / 60);
    diff.min = tmp % 60;

    tmp = Math.floor((tmp - diff.min) / 60);
    diff.hour = tmp % 24;

    tmp = Math.floor((tmp - diff.hour) / 24);
    diff.day = tmp;

    return diff;
  }

  async isCheck() {
    const checkboxes = document.querySelectorAll('.checkboxes');
    checkboxes.forEach((checkbox) => {
      checkbox.addEventListener('click', async () => {
        const allAttributes = checkbox.attributes;
        if (allAttributes[3].value === 'noChecked') {
          allAttributes[3].value = 'checked';
          const datas = {
            is_pay: 1,
            id: allAttributes[2].value
          };
          const response = await axios.post(
            'http://localhost:6002/payment',
            datas
          );
          if (response.status === 200) {
            this.run();
          }
        } else if (allAttributes[3].value === 'checked') {
          allAttributes[3].value = 'noChecked';
          const datas = {
            is_pay: 0,
            id: allAttributes[2].value
          };
          const response = await axios.post(
            'http://localhost:6002/payment',
            datas
          );
          if (response.status === 200) {
            this.run();
          }
        }
      });
    });
  }

  async render() {
    return `
      <div>${await viewNav()}</div>
      <div class="row resume">
        <div class="col-1"></div>
        <div class="col-lg-5">
          <div>${btnAddExpense()}</div>
          <div>${resumeSold()}</div>
          <div>${followUp()}</div>
        </div>
        <div class="col-1"></div>
        <div class="col-lg-5 graph">
          <div class="graphHidden">${graph()}</div>
        </div>
      </div>
      <div class="returnDashboard">
        <a class="aReturnDashboard" href="/dashboard"><button id="prettyButton">Retour au Tableau de bord >></button></a>
      </div>
    `;
  }

  async attachEventListenersResumeSold() {
    const idUser = sessionStorage.getItem('idUser');
    const response = await axios.get(`http://localhost:6002/paymentsbyid/${idUser}`);
    const elFormSold = document.querySelector('#add-sold');
    const elFormGraph = document.querySelector('.graphHidden');
    let sold = '';
    if (Object.keys(response.data).length === 0) {
      sold += `
        <div class="card-body">
          <h5 class="card-title">Vous n'avez pas de nouvelle dépense</h5>
        </div>`;
      elFormGraph.remove();
    } else if (Object.keys(response.data).length !== 0) {
      response.data.forEach((payment) => {
        if (payment.user_id_must_receive === parseInt(idUser, 10)) {
          if (payment.is_pay === 0) {
            sold += `
              <div class="card-body">
                <input class="form-check-input checkboxes" type="checkbox" value="${payment.id}" id="noChecked">
                <label class="form-check-label expense-label" for="noChecked">
                  <h5 class="card-title">${payment.object}</h5>
                  <p class="card-text money">${payment.firstname} vous doit : ${payment.price} €</p>
                </label>
              </div>
            `;
          } else if (payment.is_pay === 1) {
            sold += `
              <div class="card-body">
                <input class="form-check-input checkboxes" type="checkbox" value="${payment.id}" id="checked" checked>
                <label class="form-check-label expense-label" for="checked">
                  <h5 class="card-title">${payment.object}</h5>
                  <p class="card-text money">${payment.firstname} vous doit : ${payment.price} €</p>
                </label>
              </div>
            `;
          }
        } else if (payment.user_id_must_give === parseInt(idUser, 10)) {
          sold += `
            <div class="card-body">
              <h5 class="card-title">${payment.object}</h5>
              <p class="card-text money">Vous devez à ${payment.firstname} : ${payment.price} €</p>
            </div>
          `;
        }
      });
    }
    elFormSold.innerHTML = sold;
  }

  async attachEventListenersFollowUp() {
    const idUser = sessionStorage.getItem('idUser');
    const response = await axios.get(`http://localhost:6002/paymentsbyid/${idUser}`);
    let follow = '';
    const dateNow = new Date();
    if (Object.keys(response.data).length !== 0) {
      response.data.forEach((payment) => {
        const dateDB = new Date(payment.untilWhen);
        if (Date.parse(dateNow) >= Date.parse(dateDB) && follow === '') {
          if (payment.user_id_must_give === parseInt(idUser, 10)) {
            follow += `
              <div class="card-body noPaddingBot">
                <h5 class="card-title">Vous avez <span class="colorRed">${this.dateDiff(dateDB, dateNow).day} jours</span> de retard pour rembourser ${payment.firstname}</h5>
                <p class="card-text money">Rappel : vous lui devez ${payment.price} €</p>
              </div>
            `;
          }
        }
      });
    }
    const elFormFollowUp = document.querySelector('.followUp');
    let bodyFollow = `
        <div>
          <h2 class="h2common">Suivi de paiement</h2>
        </div>
        <div class="card h-100" id="add-follow">
        </div>
      `;
    if (follow === '') {
      bodyFollow = '';
      elFormFollowUp.innerHTML = bodyFollow;
    } else {
      elFormFollowUp.innerHTML = bodyFollow;
      const elFormFollow = document.querySelector('#add-follow');
      elFormFollow.innerHTML = follow;
    }
  }

  async showGraph() {
    const idUser = sessionStorage.getItem('idUser');
    const response = await axios.get(`http://localhost:6002/paymentsbyid/${idUser}`);
    let receive = 0;
    let give = 0;
    if (Object.keys(response.data).length !== 0) {
      response.data.forEach((payment) => {
        if (payment.user_id_must_receive === parseInt(idUser, 10)) {
          receive += payment.price;
        } else if (payment.user_id_must_give === parseInt(idUser, 10)) {
          give += payment.price;
        }
      });
      const data = [
        { year: 'Vous devez recevoir', count: receive },
        { year: 'Vous devez donner', count: give }
      ];

      new Chart(
        document.querySelector('#myChart'),
        {
          type: 'doughnut',
          data: {
            labels: data.map((row) => row.year),
            datasets: [
              {
                data: data.map((row) => row.count),
                labels: 'Test',
                backgroundColor: [
                  'rgb(0, 156, 13)',
                  'rgb(255, 57, 57)'
                ],
                hoverOffset: 4
              }
            ]
          },
          options: {
            animations: {
              tension: {
                duration: 1000,
                easing: 'linear',
                from: 1,
                to: 0,
                loop: true
              }
            },
            plugins: {
              legend: {
                labels: {
                  font: {
                    size: 18
                  }
                }
              }
            }
          }
        }
      );
    }
  }

  async run() {
    this.el.innerHTML = await this.render();
    this.attachEventListenersResumeSold();
    this.attachEventListenersFollowUp();
    await this.showGraph();
    this.isCheck();
  }
};

export default CommonExpense;
