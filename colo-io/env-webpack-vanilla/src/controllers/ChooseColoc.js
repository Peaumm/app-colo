import axios from 'axios';
import viewNav from '../views/nav/nav';
import body from '../views/choose/body';

const ChooseColoc = class ChooseColoc {
  constructor(params) {
    this.el = document.querySelector('#root');
    this.params = params;
    this.data = [];

    this.run();
  }

  async isInColoc() {
    const data = sessionStorage.getItem('idUser');

    try {
      const response = await axios.get(`http://localhost:6002/user/${data}`);
      if (response.data.coloc_id !== null) {
        window.location.href = 'http://127.0.0.1:5001/dashboard';
      } else {
        this.el.innerHTML = await this.render();
      }
      return 0;
    } catch (error) {
      return ('Error sending message to database:', error);
    }
  }

  async render() {
    return `
      <div>${await viewNav()}</div>
      ${body()}
    `;
  }

  run() {
    this.isInColoc();
  }
};

export default ChooseColoc;
