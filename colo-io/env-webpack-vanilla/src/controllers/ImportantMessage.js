import axios from 'axios';

import viewNav from '../views/nav/nav';
import body from '../views/message/body';
import returnDashboard from '../views/message/returnDashboard';

const ImportantMessage = class ImportantMessage {
  constructor(params) {
    this.el = document.querySelector('#root');
    this.params = params;
    this.data = [];

    this.run();
  }

  async sendData() {
    const elForm = document.querySelector('#add-message');
    const datasForm = new FormData(elForm);
    const idUser = sessionStorage.getItem('idUser');
    const responseColocId = await axios.get(`http://localhost:6002/user/${idUser}`);

    try {
      const datas = {
        title: datasForm.get('title'),
        content: datasForm.get('content'),
        coloc_id: responseColocId.data.coloc_id
      };
      const response = await axios.post(
        'http://localhost:6002/messages',
        datas
      );
      if (response.status === 200) {
        window.location.href = 'http://127.0.0.1:5001/dashboard';
      }
      return 0;
    } catch (error) {
      return ('Error sending message to database:', error);
    }
  }

  submitForm() {
    const elForm = document.querySelector('#add-message');

    elForm.addEventListener('submit', (e) => {
      e.preventDefault();
      this.sendData();
    });
  }

  async render() {
    return `
      <div>${await viewNav()}</div>
      ${returnDashboard()}
      ${body()}
    `;
  }

  async run() {
    this.el.innerHTML = await this.render();
    this.submitForm();
  }
};

export default ImportantMessage;
