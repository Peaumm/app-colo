import axios from 'axios';
import viewNav from '../views/nav/nav';
import btnAddTask from '../views/task/btnAddTask';
import taskFinish from '../views/task/taskFinish';
import taskInProgress from '../views/task/taskInProgress';

const Task = class Task {
  constructor(params) {
    this.el = document.querySelector('#root');
    this.params = params;
    this.data = [];

    this.run();
  }

  newDate(date) {
    const dateTask = new Date(date);
    const options = { month: 'long' };
    const month = new Intl.DateTimeFormat('fr-FR', options).format(dateTask);
    const year = dateTask.getFullYear();
    const day = dateTask.getDate();

    const fullDate = `${day} ${month} ${year}`;
    return fullDate;
  }

  async isCheck() {
    const checkboxes = document.querySelectorAll('.checkboxes');
    checkboxes.forEach((checkbox) => {
      checkbox.addEventListener('click', async () => {
        const allAttributes = checkbox.attributes;
        if (allAttributes[3].value === 'noChecked') {
          allAttributes[3].value = 'checked';
          const datas = {
            finished: 1,
            id: allAttributes[2].value
          };
          const response = await axios.post(
            'http://localhost:6002/task',
            datas
          );
          if (response.status === 200) {
            this.run();
          }
        } else if (allAttributes[3].value === 'checked') {
          allAttributes[3].value = 'noChecked';
          const datas = {
            finished: 0,
            id: allAttributes[2].value
          };
          const response = await axios.post(
            'http://localhost:6002/task',
            datas
          );
          if (response.status === 200) {
            this.run();
          }
        }
      });
    });
  }

  async render() {
    return `
      <div>${await viewNav()}</div>
      ${btnAddTask()}
      <div id="notTask"></div>
      <div class="row">
        <div class="col-1"></div>
        <div class="col-lg-5" id="tasks">${taskInProgress()}</div>
        <div class="col-lg-5" id="tasks">${taskFinish()}</div>
      </div>
      <div class="returnDashboard">
        <a class="aReturnDashboard" href="/dashboard"><button id="prettyButton">Retour au Tableau de bord >></button></a>
      </div>
    `;
  }

  async attachEventListeners() {
    const idUser = sessionStorage.getItem('idUser');
    const response = await axios.get(`http://localhost:6002/tasksbyid/${idUser}`);
    const elPrgress = document.querySelector('#progress');
    const elFinished = document.querySelector('#finished');
    const elNotTask = document.querySelector('#notTask');
    const elParentFinished = document.querySelector('#parentFinished');
    const elParentProgress = document.querySelector('#parentProgress');

    let progress = '';
    let finished = '';
    if (Object.keys(response.data).length === 0) {
      elNotTask.innerHTML += `
        <div class="container">
          <div class="card text-center">
            <p>Vous n'avez pas de nouvelle tâche</p>
          </div>
        </div>
      `;
      elParentFinished.remove();
      elParentProgress.remove();
    } else if (Object.keys(response.data).length !== 0) {
      response.data.forEach((tasks) => {
        if (tasks.finished === 0) {
          progress += `
            <div class="form-check">
              <input class="form-check-input checkboxes" type="checkbox" value="${tasks.id}" id="noChecked">
              <label class="form-check-label" for="noChecked">
                ${tasks.name} pour le ${this.newDate(tasks.untilWhen)}
              </label>
            </div>
          `;
        } else if (tasks.finished === 1) {
          finished += `
            <div class="form-check">
              <input class="form-check-input checkboxes" type="checkbox" value="${tasks.id}" id="checked" checked>
              <label class="form-check-label" for="checked">
                ${tasks.name} pour le ${this.newDate(tasks.untilWhen)}
              </label>
            </div>
          `;
        }
      });
    }
    elPrgress.innerHTML = progress;
    elFinished.innerHTML = finished;
  }

  async run() {
    this.el.innerHTML = await this.render();
    await this.attachEventListeners();
    this.isCheck();
  }
};

export default Task;
