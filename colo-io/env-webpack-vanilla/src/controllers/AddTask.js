import axios from 'axios';
import { multipleSelect } from 'multiple-select-vanilla';
import viewNav from '../views/nav/nav';
import body from '../views/addTask/bodyAddTask';
import returnTask from '../views/addTask/returnTask';

const AddTask = class AddTask {
  constructor(params) {
    this.el = document.querySelector('#root');
    this.params = params;
    this.data = [];
    this.msUser = [];
    this.msTask = [];

    this.run();
  }

  async sendData() {
    const elForm = document.querySelector('#add-task-form');
    const datasForm = new FormData(elForm);
    const idUser = sessionStorage.getItem('idUser');

    try {
      this.msUser.getSelects().forEach(async (idUserReceive) => {
        const datas = {
          name: this.msTask.getSelects()[0],
          user_id_give_task: idUser,
          user_id_receive_task: idUserReceive,
          untilWhen: datasForm.get('date')
        };
        const response = await axios.post(
          'http://localhost:6002/tasks',
          datas
        );
        if (response.status === 200) {
          window.location.href = 'http://127.0.0.1:5001/task';
        }
      });
      return 0;
    } catch (error) {
      return ('Error sending message to database:', error);
    }
  }

  submitForm() {
    const elForm = document.querySelector('#add-task-form');

    elForm.addEventListener('submit', (e) => {
      e.preventDefault();
      this.sendData();
    });
  }

  async render() {
    return `
      <div>${await viewNav()}</div>
      ${returnTask()}
      ${body()}
    `;
  }

  async attachEventListeners() {
    const idUser = sessionStorage.getItem('idUser');
    const responseColocId = await axios.get(`http://localhost:6002/user/${idUser}`);
    const responseNotId = await axios.get(`http://localhost:6002/usersnot/${idUser}/${responseColocId.data.coloc_id}`);
    this.fillSelectUser(responseNotId.data);
  }

  async fillSelectUser(users) {
    const idUser = sessionStorage.getItem('idUser');
    const responseUser = await axios.get(`http://localhost:6002/user/${idUser}`);
    if (Object.keys(users).length !== 0) {
      const userDatas = users.map((user) => ({
        text: `${user.firstname} ${user.lastname}`,
        value: user.id
      }));
      userDatas.push({
        text: `${responseUser.data.firstname} ${responseUser.data.lastname} (moi)`,
        value: responseUser.data.id
      });
      this.msUser = multipleSelect('.multiple-select', {
        name: 'idReceive',
        single: true,
        maxHeight: 4,
        maxHeightUnit: 'row',
        data: userDatas
      });
    } else {
      const elFormNotUser = document.querySelector('#notUser');
      elFormNotUser.classList.add('mb-3');
      const select = '<option disabled selected>Il y a personne avec vous dans la colocation. Veuillez ajouter un autre colocataire pour ajouter une dépense.</option>';
      elFormNotUser.innerHTML = select;
    }
  }

  async fillSelectTasks() {
    const tasks = [
      'Faire le menage',
      'Faire la vaisselle',
      'Faire les courses',
      'Préparer à manger',
      'Sortir les poubelles'
    ];

    const taskDatas = tasks.map((task) => ({
      text: task,
      value: task
    }));
    this.msTask = multipleSelect('.multiple-select-task', {
      name: 'task',
      single: true,
      maxHeight: 3,
      maxHeightUnit: 'row',
      data: taskDatas
    });
  }

  async run() {
    this.el.innerHTML = await this.render();
    this.attachEventListeners();
    this.fillSelectTasks();
    this.submitForm();
  }
};

export default AddTask;
