import axios from 'axios';
import { multipleSelect } from 'multiple-select-vanilla';
import viewNav from '../views/nav/nav';
import bodyAddExpense from '../views/addExpense/bodyAddExpense';
import returnExpense from '../views/addExpense/returnExpense';

const AddExpense = class AddExpense {
  constructor(params) {
    this.el = document.querySelector('#root');
    this.params = params;
    this.data = [];
    this.ms1 = [];

    this.run();
  }

  async sendData() {
    const elForm = document.querySelector('#add-expense-form');
    const datasForm = new FormData(elForm);
    const idUser = sessionStorage.getItem('idUser');

    try {
      this.ms1.getSelects().forEach(async (idUserGive) => {
        const datas = {
          object: datasForm.get('object'),
          user_id_must_receive: idUser,
          price: datasForm.get('price') / this.ms1.getSelects().length,
          user_id_must_give: idUserGive,
          untilWhen: datasForm.get('date')
        };
        const response = await axios.post(
          'http://localhost:6002/payments',
          datas
        );
        if (response.status === 200) {
          window.location.href = 'http://127.0.0.1:5001/commonexpense';
        }
      });
      return 0;
    } catch (error) {
      return ('Error sending message to database:', error);
    }
  }

  submitForm() {
    const elForm = document.querySelector('#add-expense-form');

    elForm.addEventListener('submit', (e) => {
      e.preventDefault();
      this.sendData();
    });
  }

  async render() {
    return `
      <div>${await viewNav()}</div>
      ${returnExpense()}
      ${bodyAddExpense()}
    `;
  }

  async attachEventListeners() {
    const idUser = sessionStorage.getItem('idUser');
    const responseColocId = await axios.get(`http://localhost:6002/user/${idUser}`);
    const responseNotId = await axios.get(`http://localhost:6002/usersnot/${idUser}/${responseColocId.data.coloc_id}`);
    this.fillSelect(responseNotId.data);
  }

  async fillSelect(users) {
    const idUser = sessionStorage.getItem('idUser');
    const responseUser = await axios.get(`http://localhost:6002/user/${idUser}`);
    if (Object.keys(users).length !== 0) {
      const userDatas = users.map((user) => ({
        text: `${user.firstname} ${user.lastname}`,
        value: user.id
      }));
      userDatas.push({
        text: `${responseUser.data.firstname} ${responseUser.data.lastname} (moi)`,
        value: responseUser.data.id
      });
      this.ms1 = multipleSelect('.multiple-select', {
        name: 'idGive',
        single: false,
        maxHeight: 4,
        maxHeightUnit: 'row',
        data: userDatas
      });
    } else {
      const elFormNotUser = document.querySelector('#notUser');
      elFormNotUser.classList.add('mb-3');
      const select = '<option disabled selected>Il y a personne avec vous dans la colocation. Veuillez ajouter un autre colocataire pour ajouter une dépense.</option>';
      elFormNotUser.innerHTML = select;
    }
  }

  async run() {
    this.el.innerHTML = await this.render();
    this.attachEventListeners();
    this.submitForm();
  }
};

export default AddExpense;
