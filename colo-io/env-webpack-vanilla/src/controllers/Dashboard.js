import axios from 'axios';
import { Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import frLocale from '@fullcalendar/core/locales/fr';

import viewNav from '../views/nav/nav';
import nextTask from '../views/dashboard/nextTask';
import calendarBody from '../views/dashboard/calendar';
import viewPayment from '../views/dashboard/payment';
import importantMessage from '../views/dashboard/importantMessage';

const Dashboard = class Dashboard {
  constructor(params) {
    this.el = document.querySelector('#root');
    this.params = params;
    this.data = [];

    this.run();
  }

  session() {
    if (sessionStorage.getItem('idUser') === null) {
      window.location.href = 'http://127.0.0.1:5001/';
    }
  }

  newDate(date) {
    const dateTask = new Date(date);
    const options = { month: 'long' };
    const month = new Intl.DateTimeFormat('fr-FR', options).format(dateTask);
    const year = dateTask.getFullYear();
    const day = dateTask.getDate();

    const fullDate = `${day} ${month} ${year}`;
    return fullDate;
  }

  newDateHide() {
    const dateTask = new Date();
    const month = dateTask.getMonth() + 1;
    const year = dateTask.getFullYear();
    const day = dateTask.getDate();

    const fullDate = `${year}-${month}-${day}`;
    return fullDate;
  }

  async allTask() {
    const idUser = sessionStorage.getItem('idUser');
    const responseColocId = await axios.get(`http://localhost:6002/user/${idUser}`);
    const responseTasks = await axios.get(`http://localhost:6002/tasksbycolocid/${responseColocId.data.coloc_id}`);
    const tasksDatas = [];
    if (Object.keys(responseTasks.data).length !== 0) {
      responseTasks.data.forEach((task) => {
        if (task.finished === 1) {
          tasksDatas.push({
            title: `${task.name}`,
            start: `${task.untilWhen}`,
            eventClassNames: 'finished'
          });
        } else if (task.finished === 0) {
          tasksDatas.push({
            title: `${task.name}`,
            start: `${task.untilWhen}`,
            eventClassNames: 'notFinished'
          });
        }
      });
    }
    return tasksDatas;
  }

  async hideTask() {
    const idUser = sessionStorage.getItem('idUser');
    const responseColocId = await axios.get(`http://localhost:6002/user/${idUser}`);
    const responseTasks = await axios.get(`http://localhost:6002/tasksbycolocid/${responseColocId.data.coloc_id}`);
    if (Object.keys(responseTasks.data).length !== 0) {
      responseTasks.data.forEach(async (task) => {
        if ((Date.parse(this.newDateHide()) >= Date.parse(task.untilWhen) && task.finished === 1)) {
          const datas = {
            is_hide: 1,
            id: task.id
          };
          await axios.post('http://localhost:6002/taskhide', datas);
        }
      });
    }
  }

  async hideExpense() {
    const idUser = sessionStorage.getItem('idUser');
    const responseExpenses = await axios.get(`http://localhost:6002/paymentsbyid/${idUser}`);
    if (Object.keys(responseExpenses.data).length !== 0) {
      responseExpenses.data.forEach(async (payment) => {
        if ((Date.parse(this.newDateHide()) >= Date.parse(payment.untilWhen)
          && payment.is_pay === 1)) {
          const datas = {
            is_hide: 1,
            id: payment.id
          };
          await axios.post('http://localhost:6002/paymenthide', datas);
        }
      });
    }
  }

  async showNameColoc() {
    const idUser = sessionStorage.getItem('idUser');
    const responseColocId = await axios.get(`http://localhost:6002/user/${idUser}`);
    const nameColoc = await axios.get(`http://localhost:6002/coloc/${responseColocId.data.coloc_id}`);
    return nameColoc.data.name;
  }

  async showMember() {
    const idUser = sessionStorage.getItem('idUser');
    const responseColocId = await axios.get(`http://localhost:6002/user/${idUser}`);
    const responseNotId = await axios.get(`http://localhost:6002/usersnot/${idUser}/${responseColocId.data.coloc_id}`);
    let users = 'Les autres membres de la colocation :<div class="row"><div class="col-6">';
    let i = 0;
    if (Object.keys(responseNotId.data).length !== 0) {
      responseNotId.data.forEach((user) => {
        if (Object.keys(responseNotId.data).length % 2 === 0) {
          if (Math.floor(Object.keys(responseNotId.data).length / 2) !== i) {
            users += `<li>${user.firstname} ${user.lastname}</li>`;
            i += 1;
            if (i === Object.keys(responseNotId.data).length / 2) {
              users += '</div><div class="col-6">';
            }
          } else {
            users += `<li>${user.firstname} ${user.lastname}</li>`;
          }
        } else if (Object.keys(responseNotId.data).length % 2 !== 0) {
          if (Math.floor((Object.keys(responseNotId.data).length / 2 + 1)) !== i) {
            users += `<li>${user.firstname} ${user.lastname}</li>`;
            i += 1;
            if (Math.floor(Object.keys(responseNotId.data).length / 2 + 1) === i) {
              users += '</div><div class="col-6">';
            }
          } else {
            users += `<li>${user.firstname} ${user.lastname}</li>`;
          }
        }
      });
    } else if (Object.keys(responseNotId.data).length === 0) {
      users = '<li>Vous êtes tout seul dans cette colocation</li>';
    }
    users += '</div></div>';
    return users;
  }

  async render() {
    return `
      <div>${await viewNav()}</div>
      <div id="name-coloc">Bienvenue dans ${await this.showNameColoc()}</div>
      <div class="row">
        <div class="col-xxl-7 row">
          <div class="col-md-7">${nextTask()}</div>
          <div class="col-md-5">${viewPayment()}</div>
          ${importantMessage()}
        </div>
        <div class="column col-xxl-5">
          <div class="calendar-hidden">${calendarBody()}</div>
          <div id="member">${await this.showMember()}</div>
        </div>
      </div>
    `;
  }

  async attachEventListenersTask() {
    const idUser = sessionStorage.getItem('idUser');
    const response = await axios.get(`http://localhost:6002/tasksbyidlimitless/${idUser}`);
    const elFormSold = document.querySelector('#task');
    let task = '';
    if (Object.keys(response.data).length === 0) {
      task += `
        <div class="card-body">
          <h5 class="card-title">Vous n'avez pas de nouvelle tâche</h5>
        </div>`;
    } else if (Object.keys(response.data).length !== 0) {
      response.data.forEach((tasks) => {
        if (tasks.user_id_receive_task === parseInt(idUser, 10)) {
          task += `
            <div class="card-body">
              <h5 class="card-title">${tasks.name}</h5>
              <p class="card-text">Saisie par ${tasks.firstname} et à faire avant le ${this.newDate(tasks.untilWhen)}</p>
            </div>
          `;
        }
      });
    }
    elFormSold.innerHTML = task;
  }

  async attachEventListenersResumeSold() {
    const idUser = sessionStorage.getItem('idUser');
    const response = await axios.get(`http://localhost:6002/paymentsbyidlimitless/${idUser}`);
    const elFormSold = document.querySelector('#sold');
    let sold = '';
    if (Object.keys(response.data).length === 0) {
      sold += `
        <div class="card-body">
          <h5 class="card-title">Vous n'avez pas de nouvelle dépense</h5>
        </div>`;
    } else if (Object.keys(response.data).length !== 0) {
      response.data.forEach((payments) => {
        if (payments.user_id_must_receive === parseInt(idUser, 10)) {
          sold += `
            <div class="card-body">
              <h5 class="card-title">${payments.firstname} vous doit :</h5>
              <p class="card-text">${payments.price} €</p>
            </div>
          `;
        } else if (payments.user_id_must_give === parseInt(idUser, 10)) {
          sold += `
            <div class="card-body">
              <h5 class="card-title">Vous devez à ${payments.firstname} :</h5>
              <p class="card-text">${payments.price} €</p>
            </div>
          `;
        }
      });
    }
    elFormSold.innerHTML = sold;
  }

  async attachEventListenersMessages() {
    const idUser = sessionStorage.getItem('idUser');
    const responseUser = await axios.get(`http://localhost:6002/user/${idUser}`);
    const response = await axios.get(`http://localhost:6002/messages/${responseUser.data.coloc_id}`);
    const elFormMessage = document.querySelector('#messages');
    let messages = '';
    if (Object.keys(response.data).length === 0) {
      messages += `
        <div class="card-body">
          <h5 class="card-title">Vous n'avez pas de nouveaux messages</h5>
        </div>`;
    } else if (Object.keys(response.data).length !== 0) {
      response.data.forEach((message) => {
        messages += `
          <div class="card-body">
            <h5 class="card-title">Titre : ${message.title}</h5>
            <p class="card-text">${message.content}</p>
          </div>`;
      });
    }
    elFormMessage.innerHTML = messages;
  }

  async showCalendar() {
    const allTasks = await this.allTask();
    const calendarEl = document.getElementById('calendar');
    const calendar = new Calendar(calendarEl, {
      plugins: [dayGridPlugin, timeGridPlugin, listPlugin],
      initialView: 'dayGridMonth',
      headerToolbar: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,listWeek'
      },
      events: allTasks,
      locales: [frLocale],
      locale: 'fr'
    });
    calendar.render();
  }

  async run() {
    this.el.innerHTML = await this.render();
    this.attachEventListenersResumeSold();
    this.attachEventListenersTask();
    this.attachEventListenersMessages();
    await this.showCalendar();
    this.hideTask();
    this.hideExpense();
  }
};

export default Dashboard;
