<?php

require 'vendor/autoload.php';

use App\Router;
use App\Controllers\Users;
use App\Controllers\User;
use App\Controllers\Colocs;
use App\Controllers\UserColocId;
use App\Controllers\Coloc;
use App\Controllers\UserPassword;
use App\Controllers\PasswordVerify;
use App\Controllers\Payments;
use App\Controllers\UsersNot;
use App\Controllers\PaymentsByUserId;
use App\Controllers\PaymentsByUserIdLimitless;
use App\Controllers\Tasks;
use App\Controllers\TasksByUserId;
use App\Controllers\TasksByUserIdLimitless;
use App\Controllers\Task;
use App\Controllers\TasksByColocId;
use App\Controllers\TaskHide;
use App\Controllers\Messages;
use App\Controllers\MessagesByColocId;
use App\Controllers\Payment;
use App\Controllers\PaymentHide;


new Router([
  'users' => Users::class,
  'user/:id' => User::class,
  'colocs' => Colocs::class,
  'usercolocid' => UserColocId::class,
  'coloc/:id' => Coloc::class,
  'userpassword/:id' => UserPassword::class,
  'passwordverify' => PasswordVerify::class,
  'payments' => Payments::class,
  'usersnot/:id/:coloc_id' => UsersNot::class,
  'paymentsbyid/:id' => PaymentsByUserId::class,
  'paymentsbyidlimitless/:id' => PaymentsByUserIdLimitless::class,
  'tasks' => Tasks::class,
  'tasksbyid/:id' => TasksByUserId::class,
  'tasksbyidlimitless/:id' => TasksByUserIdLimitless::class,
  'task' => Task::class,
  'tasksbycolocid/:coloc_id' => TasksByColocId::class,
  'taskhide' => TaskHide::class,
  'messages' => Messages::class,
  'messages/:coloc_id' => MessagesByColocId::class,
  'payment' => Payment::class,
  'paymenthide' => PaymentHide::class,
]);