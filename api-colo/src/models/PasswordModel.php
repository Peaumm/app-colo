<?php

namespace App\Models;

use \PDO;
use stdClass;

class PasswordModel extends SqlConnect {
  public function post($data) {
    $req = $this->db->prepare("SELECT password FROM users WHERE id=:id");
    $req->execute(["id" => $data['id']]);
    $pass = $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : new stdClass();
    $passwordVerify = password_verify($data['password'], $pass[0]['password']);
    if ($passwordVerify === true) {
      return '200';
    } else if ($passwordVerify === false){
      return '401';
    }
    
  }
}
