<?php

namespace App\Models;

use \PDO;
use stdClass;

class PaymentModel extends SqlConnect {
  public function getAll() {
    $req = $this->db->prepare("SELECT * FROM payments");
    $req->execute();

    return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function get($id) {
    $req = $this->db->prepare("SELECT * FROM payments WHERE id=:id");
    $req->execute(["id" => $id]);

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function getPaymentsByUserId($id) {
    $query = "
      SELECT
        p.*, u.firstname
      FROM
        (SELECT * FROM payments)AS p
      CROSS JOIN
        (SELECT firstname, id FROM users) AS u
      WHERE 
        (p.user_id_must_receive=:id OR p.user_id_must_give=:id) AND 
          ((u.id=p.user_id_must_receive OR u.id=p.user_id_must_give) AND
          (u.id!=:id OR u.id!=:id) AND p.is_hide=0)
      ORDER BY `payments`.`untilWhen` ASC
      ;";
    $req = $this->db->prepare($query);
    $req->execute(["id" => $id]);

    return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function getPaymentsByUserIdLimitless3($id) {
    $query = "
      SELECT
        p.*, u.firstname
      FROM
        (SELECT * FROM payments)AS p
      CROSS JOIN
        (SELECT firstname, id FROM users) AS u
      WHERE 
        (p.user_id_must_receive=:id OR p.user_id_must_give=:id) AND 
          ((u.id=p.user_id_must_receive OR u.id=p.user_id_must_give) AND
          (u.id!=:id OR u.id!=:id) AND p.is_pay=0)
      ORDER BY `payments`.`untilWhen` ASC LIMIT 3
      ;";
    $req = $this->db->prepare($query);
    $req->execute([
      "id" => $id
    ]);

    return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function getLast() {
    $req = $this->db->prepare("SELECT * FROM payments ORDER BY id DESC LIMIT 1");
    $req->execute();

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function post($data) {
    $query = "
      INSERT INTO payments (object, user_id_must_receive, price, user_id_must_give, untilWhen)
      VALUES (?, ?, ?, ?, ?)
    ";

    $req = $this->db->prepare($query);
    $req->execute([
      $data['object'],
      $data['user_id_must_receive'],
      $data['price'],
      $data['user_id_must_give'],
      $data['untilWhen']
    ]);
  }

  public function updatePay($data) {
    $req = $this->db->prepare("UPDATE payments SET is_pay=:is_pay WHERE id=:id");
    $req->execute($data);
  }

  public function updateIsHide($data) {
    $req = $this->db->prepare("UPDATE payments SET is_hide=:is_hide WHERE id=:id");
    $req->execute($data);
  }
}