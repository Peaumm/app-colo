<?php

namespace App\Models;

use \PDO;
use stdClass;

class MessageModel extends SqlConnect {
  public function getAll() {
    $req = $this->db->prepare("SELECT * FROM messages");
    $req->execute();

    return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function get($coloc_id) {
    $req = $this->db->prepare("SELECT * FROM messages WHERE coloc_id=:coloc_id LIMIT 2");
    $req->execute(["coloc_id" => $coloc_id]);

    return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function post($data) {
    $query = "
      INSERT INTO messages (title, content, coloc_id)
      VALUES (?, ?, ?)
    ";

    $req = $this->db->prepare($query);
    $req->execute([
      $data['title'],
      $data['content'],
      $data['coloc_id']
    ]);
  }

  public function getLast() {
    $req = $this->db->prepare("SELECT * FROM coloc ORDER BY id DESC LIMIT 1");
    $req->execute();

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }
}