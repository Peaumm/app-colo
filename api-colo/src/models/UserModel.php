<?php

namespace App\Models;

use \PDO;
use stdClass;

class UserModel extends SqlConnect {
  public function getAll() {
    $req = $this->db->prepare("SELECT * FROM users");
    $req->execute();

    return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function get($id) {
    $req = $this->db->prepare("SELECT * FROM users WHERE id=:id");
    $req->execute(["id" => $id]);

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function getNotId($id, $coloc_id) {
    $req = $this->db->prepare("SELECT * FROM users WHERE id!=:id && coloc_id=:coloc_id");
    $req->execute([
      "id" => $id,
      "coloc_id" => $coloc_id
    ]);

    return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function getLast() {
    $req = $this->db->prepare("SELECT * FROM users ORDER BY id DESC LIMIT 1");
    $req->execute();

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function post($data) {
    $query = "
      INSERT INTO users (firstname, lastname, email, date_birthday, password)
      VALUES (?, ?, ?, ?, ?)
    ";

    $req = $this->db->prepare($query);
    $req->execute([
      $data['firstname'],
      $data['lastname'],
      $data['email'],
      $data['date_birthday'],
      password_hash($data['password'], PASSWORD_BCRYPT)
    ]);
  }

  public function updateUserColocId($data) {
    $req = $this->db->prepare("UPDATE users SET coloc_id=:coloc_id WHERE id=:id");
    $req->execute($data);
  }

  public function updateUser($data, $id) {
    $req = $this->db->prepare("UPDATE users SET firstname=:firstname, lastname=:lastname, email=:email, date_birthday=:date_birthday WHERE id=$id");
    $req->execute($data);
  }

  public function updateUserPassword($data, $id) {
    $newPassword = password_hash($data['password'], PASSWORD_BCRYPT);
    $req = $this->db->prepare("UPDATE users SET `password`=:password WHERE id=$id");
    $req->bindValue(':password', $newPassword, PDO::PARAM_STR);
    $req->execute();
  }
}