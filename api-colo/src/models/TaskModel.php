<?php

namespace App\Models;

use \PDO;
use stdClass;

class TaskModel extends SqlConnect {
  public function getAll() {
    $req = $this->db->prepare("SELECT * FROM tasks");
    $req->execute();

    return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function get($id) {
    $req = $this->db->prepare("SELECT * FROM tasks WHERE id=:id");
    $req->execute(["id" => $id]);

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function getTasksByUserId($id) {
    $query = "
      SELECT
        t.*, u.firstname
      FROM
        (SELECT * FROM tasks)AS t
      CROSS JOIN
        (SELECT firstname, id FROM users) AS u
      WHERE
        (((t.user_id_receive_task=:id) AND 
          ((u.id=t.user_id_receive_task OR u.id=t.user_id_give_task) AND
          (u.id!=:id)) OR
        (t.user_id_receive_task=t.user_id_give_task AND u.id=t.user_id_give_task AND u.id=:id)) AND
        t.is_hide=0)
      ORDER BY `t`.`untilWhen` ASC
    ;";
    $req = $this->db->prepare($query);
    $req->execute(["id" => $id]);

    return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function getTasksByUserIdLimitless2($id) {
    $query = "
      SELECT
        t.*, u.firstname
      FROM
        (SELECT * FROM tasks)AS t
      CROSS JOIN
        (SELECT firstname, id FROM users) AS u
      WHERE
        (((t.user_id_receive_task=:id) AND 
          ((u.id=t.user_id_receive_task OR u.id=t.user_id_give_task) AND
          (u.id!=:id)) OR
        (t.user_id_receive_task=t.user_id_give_task AND u.id=t.user_id_give_task AND u.id=:id)) AND
        t.finished=0)
      ORDER BY `t`.`untilWhen` ASC LIMIT 2
    ;";
    $req = $this->db->prepare($query);
    $req->execute([
      "id" => $id
    ]);

    return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function getTasksByColocId($coloc_id) {
    $query = "
      SELECT
        t.*, u.firstname
      FROM
        (SELECT * FROM tasks)AS t
      CROSS JOIN
        (SELECT firstname, id, coloc_id FROM users) AS u
      WHERE 
        (u.coloc_id=:coloc_id) && 
        (u.id=t.user_id_receive_task)
      ORDER BY `tasks`.`untilWhen` ASC
      ;";
    $req = $this->db->prepare($query);
    $req->execute([
      "coloc_id" => $coloc_id
    ]);

    return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function getLast() {
    $req = $this->db->prepare("SELECT * FROM tasks ORDER BY id DESC LIMIT 1");
    $req->execute();

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function post($data) {
    $query = "
      INSERT INTO tasks (name, user_id_give_task, user_id_receive_task, untilWhen)
      VALUES (?, ?, ?, ?)
    ";

    $req = $this->db->prepare($query);
    $req->execute([
      $data['name'],
      $data['user_id_give_task'],
      $data['user_id_receive_task'],
      $data['untilWhen']
    ]);
  }

  public function updateFinished($data) {
    $req = $this->db->prepare("UPDATE tasks SET finished=:finished WHERE id=:id");
    $req->execute($data);
  }

  public function updateIsHide($data) {
    $req = $this->db->prepare("UPDATE tasks SET is_hide=:is_hide WHERE id=:id");
    $req->execute($data);
  }
}