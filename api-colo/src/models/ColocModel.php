<?php

namespace App\Models;

use \PDO;
use stdClass;

class ColocModel extends SqlConnect {
  public function getAll() {
    $req = $this->db->prepare("SELECT * FROM coloc");
    $req->execute();

    return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function get($id) {
    $req = $this->db->prepare("SELECT * FROM coloc WHERE id=:id");
    $req->execute(["id" => $id]);

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function post($data) {
    $query = "
      INSERT INTO coloc (name, code)
      VALUES (?, ?)
    ";

    $req = $this->db->prepare($query);
    $req->execute([
      $data['name'],
      $data['code']
    ]);
  }

  public function getLast() {
    $req = $this->db->prepare("SELECT * FROM coloc ORDER BY id DESC LIMIT 1");
    $req->execute();

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

  // public function getId($code) {
  //   $req = $this->db->prepare("SELECT id FROM coloc WHERE code=:code");
  //   $req->execute(['code' => $code]);

  //   return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  // }
}