<?php

namespace App\Controllers;

use App\Models\UserModel;
use App\Controllers\Controller;

class User extends Controller{
  protected array $params;
  protected string $reqMethod;
  protected object $user;

  public function __construct($params) {
    $this->params = $params;
    $this->reqMethod = strtolower($_SERVER['REQUEST_METHOD']);
    $this->user = new UserModel();

    parent::__construct($params);
  }

  public function getUser() {
    return $this->user->get(intval($this->params['id']));
  }

  public function postUser() {
    $this->user->updateUser($this->body, intval($this->params['id']));

    return $this->user->getLast();
  }

  protected function header()
  {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: Content-Type");
    header("Access-Control-Allow-Methods: PUT, DELETE, PATCH, POST, OPTIONS");
    header('Content-type: application/json; charset=utf-8');

    if ($this->reqMethod === 'options') {
      header('Access-Control-Max-Age: 86400');
      exit;
    }
  }

  protected function ifMethodExist() {
    $method = $this->reqMethod.'User';

    if (method_exists($this, $method)) {
      echo json_encode($this->$method());

      return;
    }

    header('HTTP/1.0 404 Not Found');
    echo json_encode([
      'code' => '404',
      'message' => 'Not Found'
    ]);

    return;
  }

  protected function run() {
    $this->header();
    $this->ifMethodExist();
  }
}
