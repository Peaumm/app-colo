<?php

namespace App\Controllers;

use App\Models\UserModel;
use App\Controllers\Controller;

class UserPassword extends Controller{
  protected array $params;
  protected string $reqMethod;
  protected object $userpassword;

  public function __construct($params) {
    $this->params = $params;
    $this->reqMethod = strtolower($_SERVER['REQUEST_METHOD']);
    $this->userpassword = new UserModel();

    parent::__construct($params);
  }

  public function postUserPassword() {
    if (strlen($this->body['password']) >= 8) {
      $this->userpassword->updateUserPassword($this->body, intval($this->params['id']));
    } else {
      return 403;
    }
  }

  protected function header()
  {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: Content-Type");
    header("Access-Control-Allow-Methods: PUT, DELETE, PATCH, POST, OPTIONS");
    header('Content-type: application/json; charset=utf-8');

    if ($this->reqMethod === 'options') {
      header('Access-Control-Max-Age: 86400');
      exit;
    }
  }

  protected function ifMethodExist() {
    $method = $this->reqMethod.'UserPassword';

    if (method_exists($this, $method)) {
      echo json_encode($this->$method());

      return;
    }

    header('HTTP/1.0 404 Not Found');
    echo json_encode([
      'code' => '404',
      'message' => 'Not Found'
    ]);

    return;
  }

  protected function run() {
    $this->header();
    $this->ifMethodExist();
  }
}
